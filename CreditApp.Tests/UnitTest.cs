using CreditApp.Business.CQRS.Commands;
using CreditApp.Business.CQRS.Handlers;
using CreditApp.Business.CQRS.Queries;
using CreditApp.Business.Response;
using CreditApp.DAL.DatabaseContext;
using CreditApp.DAL.Repositories.Abstract;
using CreditApp.DAL.Repositories.Concrete;

namespace CreditApp.Tests
{
    public class UnitTest
    {
        /// <summary>
        /// Gerekli injectionlar yap�l�yor ve command,queries ler �zerinden unit test yap�l�yor
        /// </summary>

        public static ICreditApplicationRepository _creditApplicationRepository = new CreditApplicationRepository(new BankContext());
        public static ICustomerRepository _customerRepository = new CustomerRepository(new BankContext());

        [SetUp]
        public void Setup()
        {
        }

        [TestCase(3, 12.0, 50, 12)]
        [TestCase(3, 12.0, 40, 10)]
        public async Task CreateCreditApplicationTest(int customerId, double amount, int maturity, decimal interestRate)
        {

            var command = new CreateCreditApplicationCommand(customerId, amount, maturity, interestRate);
            var handler = new CreateCreditApplicationHandler(_creditApplicationRepository);
            var result = await handler.Handle(command, CancellationToken.None); 
            Assert.IsTrue(result.Success, "Successfully");
        }

        [TestCase("Mete", "Acar", "11111111111", "mete@acar.com",100,10000,5000)]
        [TestCase("Duygu", "Acar", "11111111111", "duygu@acar.com", 100, -10000, 5000)]
        public async Task CreateCustomerTest(string name, string surName, string identityNumber, string email, int creditScore, double monthlyPayment, double monthlyDebt)
        {

            var command = new CreateCustomerCommand(name, surName, identityNumber, email, creditScore, monthlyPayment, monthlyDebt);
            var handler = new CreateCustomerHandler(_customerRepository);
            var result = await handler.Handle(command, CancellationToken.None);
            //Assert.IsTrue(result.Id > 0, "Successfully");
            Assert.IsTrue(result.Success, "Successfully");
        }
        [Test]
        public async Task GetCreditApplicationListQueryTest()
        {
            var command = new GetCreditApplicationListQuery();
            var handler = new GetCreditApplicationListHandler(_creditApplicationRepository);
            var result = await handler.Handle(command, CancellationToken.None);
            //Assert.IsTrue(result.Count != 0, "Successfully");
            Assert.IsTrue(result.Success, "Successfully");
        }
        [TestCase(1)]
        public async Task GetCreditApplicationByIdTest(int id)
        {
            var command = new GetCreditApplicationByIdQuery(id);
            var handler = new GetCreditApplicationByIdHandler(_creditApplicationRepository);
            var result = await handler.Handle(command, CancellationToken.None);
            //Assert.IsTrue(result != null, "Successfully");
            Assert.IsTrue(result.Success, "Successfully");
        }


    }
}