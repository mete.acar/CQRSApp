﻿using CreditApp.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Json;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CreditApp.DAL.Dtos;

namespace CreditApp.Tests
{
    public class IntegrationTest1
    {
        /// <summary>
        /// API start ediliyor. HTTP isteği ile test ediliyor.
        /// </summary>
        /// <returns></returns>

        [Test]
        public async Task GetCreditApplications_IntegrationTests()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:7130");
            var response = await client.GetAsync("/api/CreditApplication/GetCreditApplications");
            var result = await response.Content.ReadAsStringAsync();
            Assert.IsTrue(response.IsSuccessStatusCode, "Successfully");
        }


        [TestCase(3, 12.0, 40, 10)]
        public async Task CreateCreditApplication_IntegrationTests(int customerId, double amount, int maturity, decimal interestRate)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:7130");
            var model = new CreditApplication() { CustomerId = customerId, Amount = amount, Maturity = maturity, InterestRate = interestRate };
            var response = await client.PostAsJsonAsync("/api/CreditApplication/PostCreditApplication", model);
            var result = await response.Content.ReadAsStringAsync();
            Assert.IsTrue(response.IsSuccessStatusCode, "Successfully");
        }
        [TestCase(1)]
        public async Task GetCreditApplicationById_IntegrationTests(int id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:7130");
            var response = await client.GetAsync("/api/CreditApplication/GetCreditApplicationById/" + id);
            var result = await response.Content.ReadAsStringAsync();
            Assert.IsTrue(response.IsSuccessStatusCode, "Successfully");
        }

        [TestCase("Mete", "Acar", "11111111111", "mete@acar.com", 100, 10000.15, 5000.5882)]
        [TestCase("Duygu", "Acar", "11111111111", "duygu@acar.com", 100, -10000.02, 5000.8852)]
        public async Task CreateCustomer_IntegrationTests(string name, string surName, string identityNumber, string email, int creditScore, double monthlyPayment, double monthlyDebt)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:7130");
            var model = new CreateCustomerDto() { Name = name, SurName = surName, IdentityNumber = identityNumber, Email = email, CreditScore = creditScore, MonthlyPayment = monthlyPayment, MonthlyDebt = monthlyDebt };
            var response = await client.PostAsJsonAsync("/api/Customer/AddCustomer", model);
            var result = await response.Content.ReadAsStringAsync();
            Assert.IsTrue(response.IsSuccessStatusCode, "Successfully");

        }

    }
}
