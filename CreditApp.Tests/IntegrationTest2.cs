﻿using CreditApp.DAL.Entities;  
using System.Net.Http.Json; 
using Microsoft.AspNetCore.Mvc.Testing; 

namespace CreditApp.Tests
{
    public class IntegrationTest2
    {

        private readonly HttpClient _httpClient;


        /// <summary>
        /// API start edilmeden çalışıyor.
        /// </summary>
        public IntegrationTest2()
        {
            var webApplicationFactory = new WebApplicationFactory<Program>();
            _httpClient = webApplicationFactory.CreateClient();
        }


        [Test]
        public async Task GetCreditApplications_IntegrationTests()
        {
            var response = await _httpClient.GetAsync("/api/CreditApplication/GetCreditApplications");
            var result = await response.Content.ReadAsStringAsync();
            Assert.IsTrue(response.IsSuccessStatusCode, "Successfully");
        }


        [TestCase(5, 121, 44, 10)]
        public async Task CreateCreditApplication_IntegrationTests(int customerId, double amount, int maturity, decimal interestRate)
        {
            var model = new CreditApplication() { CustomerId = customerId, Amount = amount, Maturity = maturity, InterestRate = interestRate };
            var response = await _httpClient.PostAsJsonAsync("/api/CreditApplication/PostCreditApplication", model);
            var result = await response.Content.ReadAsStringAsync();
            Assert.IsTrue(response.IsSuccessStatusCode, "Successfully");
        }
        [TestCase(1)]
        public async Task GetCreditApplicationById_IntegrationTests(int id)
        {
            var response = await _httpClient.GetAsync("/api/CreditApplication/GetCreditApplicationById/" + id);
            var result = await response.Content.ReadAsStringAsync();
            Assert.IsTrue(response.IsSuccessStatusCode, "Successfully");
        }

        [TestCase("Mete", "Acar", "11111111111", "mete@acar.com")]
        [TestCase("Duygu", "Acar", "11111111111", "duygu@acar.com")]
        public async Task CreateCustomer_IntegrationTests(string? name, string? surName, string? identityNumber, string? email)
        {
            var model = new Customer() { Name = name, SurName = surName, IdentityNumber = identityNumber, Email = email };
            var response = await _httpClient.PostAsJsonAsync("/api/Customer/AddCustomer", model);
            var result = await response.Content.ReadAsStringAsync();
            Assert.IsTrue(response.IsSuccessStatusCode, "Successfully");

        }

    }
}
