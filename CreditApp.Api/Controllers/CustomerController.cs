﻿using CreditApp.Business.CQRS.Commands;
using CreditApp.Business.Response;
using CreditApp.DAL.Dtos;
using CreditApp.DAL.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace CreditApp.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CustomerController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> AddCustomer(CreateCustomerDto model)
        { 
            var result = await _mediator.Send(new CreateCustomerCommand(model.Name, model.SurName, model.IdentityNumber, model.Email, model.CreditScore, model.MonthlyPayment, model.MonthlyDebt));
            return !result.Success ? StatusCode((int)HttpStatusCode.InternalServerError, result) : Ok(result);

        }

    }
}
