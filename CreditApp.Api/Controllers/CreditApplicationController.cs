﻿using Azure;
using Azure.Core;
using CreditApp.Business.CQRS.Commands;
using CreditApp.Business.CQRS.Queries;
using CreditApp.Business.Response;
using CreditApp.DAL.Dtos;
using CreditApp.DAL.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace CreditApp.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CreditApplicationController : ControllerBase
    {

        private readonly IMediator _mediator;
        public CreditApplicationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PostCreditApplication(CreateCreditAppDto model)
        { 
            var result = await _mediator.Send(new CreateCreditApplicationCommand(model.CustomerId, model.Amount, model.Maturity, model.InterestRate));
            return !result.Success ? StatusCode((int)HttpStatusCode.InternalServerError, result) : Ok(result);

        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ServiceResponse>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCreditApplications()
        { 
            return Ok(await _mediator.Send(new GetCreditApplicationListQuery()));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ServiceResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetCreditApplicationById(int id)
        { 
            var result = await _mediator.Send(new GetCreditApplicationByIdQuery(id));
             
            if (result.Payload == null)
            {
                return NotFound();
            }

            return !result.Success  ? StatusCode((int)HttpStatusCode.InternalServerError, result) : Ok(result);

        }

        #region Kredi Değerlendirmesini test edebilmek için ekledim ama unit testini yazmadım.
        //[HttpGet]
        //[ProducesResponseType(typeof(IEnumerable<ServiceResponse>), (int)HttpStatusCode.OK)]
        //public async Task<IActionResult> Update()
        //{
        //    //var returnModel = await _mediator.Send(new GetCreditApplicationListQuery());
        //    //return returnModel;
        //    return Ok(await _mediator.Send(new UpdateResultTypeToReviewCreditApplicationCommand()));
        //}
        #endregion


    }
}
