﻿using CreditApp.Business.Behaviors;
using CreditApp.Business.CQRS.Handlers;
using CreditApp.DAL.DatabaseContext;
using CreditApp.DAL.Repositories.Abstract;
using CreditApp.DAL.Repositories.Concrete;
using MediatR.Pipeline;
using System.Reflection;

namespace CreditApp.Api.Extension
{
    internal static class DependencyInjectionExtension
    {
        internal static void AddDependencyResolvers(this IServiceCollection services)
        {
            services.AddDbContext<BankContext>();
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(GetCreditApplicationListHandler).GetTypeInfo().Assembly)); 
            services.AddTransient(typeof(IRequestExceptionHandler<,,>), typeof(ExceptionHandlingBehavior<,,>));  
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<ICreditApplicationRepository, CreditApplicationRepository>();
        }
    }
}
