﻿namespace CreditApp.Business.Response
{
    public class ServiceResponse
    {
        public bool Success { get; init; }
        public string? Message { get; init; }
        public object? Payload { get; init; }

        public static ServiceResponse CreateSuccess(string? message)
        {
            return CreateServiceResponse(true, message, null);
        }

        public static ServiceResponse CreateSuccess(string? message, object? data)
        {
            return CreateServiceResponse(true, message, data);
        }

        public static ServiceResponse CreateExceptionError()
        {
            return CreateServiceResponse(false, ResponseMessage.ServerError, null);
        }

        public static ServiceResponse CreateError(string? message)
        {
            if (message == string.Empty)
            {
                message = ResponseMessage.ServerError;
            }

            return CreateServiceResponse(false, message, null);
        }

        private static ServiceResponse CreateServiceResponse(bool success, string? message, object? data)
        {
            return new ServiceResponse { Success = success, Message = message, Payload = data };
        }
    }

    public static class ResponseMessage
    {
        public const string ServerError = "Something went wrong. Please try again later.";
        public const string InvalidRequest = "Invalid Request";
        public const string SampleNotFound = "Sample Not Found";
        public const string AddedSuccessfully = "Successfully Added";
        public const string AddedFailed = "Added Failed";
    }
}
