﻿using CreditApp.Business.Response;
using MediatR.Pipeline;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CreditApp.Business.Behaviors
{

    public class ExceptionHandlingBehavior<TRequest, TResponse, TException> : IRequestExceptionHandler<TRequest, TResponse?, TException>
        where TRequest : IRequest<TResponse>
        where TException : Exception
        where TResponse : ServiceResponse
    {
        private readonly ILogger<ExceptionHandlingBehavior<TRequest, TResponse, TException>> logger;

        public ExceptionHandlingBehavior(
            ILogger<ExceptionHandlingBehavior<TRequest, TResponse, TException>> logger)
        {
            this.logger = logger;
        }

        public Task Handle(TRequest request, TException exception, RequestExceptionHandlerState<TResponse?> state,
            CancellationToken cancellationToken)
        {
            var error = CreateExceptionError(exception);

            logger.LogError(exception, JsonSerializer.Serialize(error));

            var response = ServiceResponse.CreateExceptionError();

            state.SetHandled(response as TResponse);

            return Task.FromResult(response);
        }

        private static ExceptionError CreateExceptionError(TException exception)
        {
            var methodName = exception.TargetSite?.DeclaringType?.DeclaringType?.FullName;
            var message = exception.Message;
            var innerException = exception.InnerException?.Message;
            var stackTrace = exception.StackTrace;

            return new ExceptionError(methodName, message, innerException, stackTrace);
        }
    }
    public class ExceptionError
    {
        public string MethodName { get; set; }
        public string Message { get; set; }
        public string InnerException { get; set; }
        public string StackTrace { get; set; }

        public ExceptionError(string? methodName, string message, string? innerException, string? stackTrace)
        {
            MethodName = methodName ?? string.Empty;
            Message = message ?? string.Empty;
            InnerException = innerException ?? string.Empty;
            StackTrace = stackTrace ?? string.Empty;
        }
    }
}
