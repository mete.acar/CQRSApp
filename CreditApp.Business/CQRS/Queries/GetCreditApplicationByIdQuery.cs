﻿using CreditApp.Business.Response;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.Business.CQRS.Queries
{
    public class GetCreditApplicationByIdQuery : IRequest<ServiceResponse>
    {
        public int Id { get; set; }
        public GetCreditApplicationByIdQuery(int id)
        {
            Id = id;
        }
    }
}
