﻿using CreditApp.Business.Response;
using CreditApp.DAL.Dtos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.Business.CQRS.Queries
{
    public class GetCreditApplicationListQuery : IRequest<ServiceResponse>
    {
    }
}
