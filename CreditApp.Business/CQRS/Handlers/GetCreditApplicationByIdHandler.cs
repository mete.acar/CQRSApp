﻿using CreditApp.Business.CQRS.Queries;
using CreditApp.Business.Response;
using CreditApp.DAL.Repositories.Abstract;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.Business.CQRS.Handlers
{
    public class GetCreditApplicationByIdHandler : IRequestHandler<GetCreditApplicationByIdQuery, ServiceResponse>
    {
        private readonly ICreditApplicationRepository _creditApplicationRepository;

        public GetCreditApplicationByIdHandler(ICreditApplicationRepository creditApplicationRepository)
        {
            _creditApplicationRepository = creditApplicationRepository;
        }

        public async Task<ServiceResponse> Handle(GetCreditApplicationByIdQuery request, CancellationToken cancellationToken)
        {
            var retunrModel =  await _creditApplicationRepository.GetCreditApplicationByIdAsync(request.Id);
            return ServiceResponse.CreateSuccess(string.Empty, retunrModel);
        }
    }
}
