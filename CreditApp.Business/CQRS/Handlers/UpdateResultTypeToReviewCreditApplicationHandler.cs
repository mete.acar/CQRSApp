﻿using CreditApp.Business.CQRS.Commands;
using CreditApp.Business.Response;
using CreditApp.DAL.Enums;
using CreditApp.DAL.Repositories.Abstract;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.Business.CQRS.Handlers
{
    public class UpdateResultTypeToReviewCreditApplicationHandler : IRequestHandler<UpdateResultTypeToReviewCreditApplicationCommand, ServiceResponse>
    {
        private readonly ICreditApplicationRepository _creditApplicationRepository;

        public UpdateResultTypeToReviewCreditApplicationHandler(ICreditApplicationRepository creditApplicationRepository)
        {
            _creditApplicationRepository = creditApplicationRepository;
        }

        public async Task<ServiceResponse> Handle(UpdateResultTypeToReviewCreditApplicationCommand request, CancellationToken cancellationToken)
        {
            var reviewAppList = await _creditApplicationRepository.GetReviewCreditApplicationList();
            foreach (var item in reviewAppList)
            {
                if (item.Customer.CreditScore < 700)     //Kredi Puanı Kontrolü
                {
                    item.Result = (int)ResultType.Reddedildi;
                   await _creditApplicationRepository.UpdateCreditApplication(item);
                    continue;
                }
                if ((item.Customer.MonthlyDebt / item.Customer.MonthlyPayment) <= 0.4) //Borç-Gelir Oranı Kontrolü
                {
                    item.Result = (int)ResultType.Reddedildi;
                    await _creditApplicationRepository.UpdateCreditApplication(item);
                    continue;
                }

                if (item.Amount > 100000) //	Kredi Miktarı Sınırlaması
                {
                    item.Result = (int)ResultType.Reddedildi;
                    await _creditApplicationRepository.UpdateCreditApplication(item);
                    continue;
                }
                if (item.Maturity > 60) //Kredi Süresi Sınırlaması
                {
                    item.Result = (int)ResultType.Reddedildi;
                    await _creditApplicationRepository.UpdateCreditApplication(item);
                    continue;
                }
                else
                {
                    item.Result = (int)ResultType.KabulEdildi;
                    await _creditApplicationRepository.UpdateCreditApplication(item);
                    continue;
                }

            }
            //return result == null ? ServiceResponse.CreateError(ResponseMessage.AddedFailed) : ServiceResponse.CreateSuccess(ResponseMessage.AddedSuccessfully);
            return ServiceResponse.CreateSuccess(ResponseMessage.AddedSuccessfully);
        }
    }
}
