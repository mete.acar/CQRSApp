﻿using CreditApp.Business.CQRS.Queries;
using CreditApp.Business.Response;
using CreditApp.DAL.Dtos;
using CreditApp.DAL.Repositories.Abstract;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.Business.CQRS.Handlers
{
    public class GetCreditApplicationListHandler : IRequestHandler<GetCreditApplicationListQuery, ServiceResponse>
    {
        private readonly ICreditApplicationRepository _creditApplicationRepository;

        public GetCreditApplicationListHandler(ICreditApplicationRepository creditApplicationRepository)
        {
            _creditApplicationRepository = creditApplicationRepository;
        }

        public async Task<ServiceResponse> Handle(GetCreditApplicationListQuery request, CancellationToken cancellationToken)
        {
            var model = await _creditApplicationRepository.GetCreditApplicationListAsync();
            return ServiceResponse.CreateSuccess(string.Empty, model);
        }
         
    }
}
