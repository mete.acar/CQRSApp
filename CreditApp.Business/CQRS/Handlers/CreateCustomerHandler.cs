﻿using CreditApp.Business.CQRS.Commands;
using CreditApp.Business.Response;
using CreditApp.DAL.Entities;
using CreditApp.DAL.Repositories.Abstract;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.Business.CQRS.Handlers
{
    public class CreateCustomerHandler : IRequestHandler<CreateCustomerCommand, ServiceResponse>
    {
        private readonly ICustomerRepository _customerRepository;

        public CreateCustomerHandler(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public async Task<ServiceResponse> Handle(CreateCustomerCommand request, CancellationToken cancellationToken)
        {
            var customer = new Customer()
            {
                Name = request.Name,
                SurName = request.SurName,
                IdentityNumber = request.IdentityNumber,
                Email = request.Email,
                CreditScore = request.CreditScore,
                MonthlyPayment = request.MonthlyPayment,
                MonthlyDebt = request.MonthlyDebt,
            };
            var result = await _customerRepository.AddCustomerAsync(customer);

            return result == null ? ServiceResponse.CreateError(ResponseMessage.AddedFailed) : ServiceResponse.CreateSuccess(ResponseMessage.AddedSuccessfully, result);
        }
    }
}
