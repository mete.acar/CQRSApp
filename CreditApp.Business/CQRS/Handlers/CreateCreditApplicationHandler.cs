﻿using CreditApp.Business.CQRS.Commands;
using CreditApp.Business.Response;
using CreditApp.DAL.Entities;
using CreditApp.DAL.Repositories.Abstract;
using CreditApp.DAL.Repositories.Concrete;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.Business.CQRS.Handlers
{
    public class CreateCreditApplicationHandler : IRequestHandler<CreateCreditApplicationCommand, ServiceResponse>
    {
        private readonly ICreditApplicationRepository _creditApplicationRepository;

        public CreateCreditApplicationHandler(ICreditApplicationRepository creditApplicationRepository)
        {
            _creditApplicationRepository = creditApplicationRepository;
        }

        public async Task<ServiceResponse> Handle(CreateCreditApplicationCommand request, CancellationToken cancellationToken)
        {
            var creditApp = new CreditApplication()
            {
                CustomerId = request.CustomerId,
                Amount = request.Amount,
                Maturity = request.Maturity,
                InterestRate = request.InterestRate,
                ApplicationDate = DateTime.Now
            };
            var result = await _creditApplicationRepository.AddCreditApplicationAsync(creditApp);

            return result == null ? ServiceResponse.CreateError(ResponseMessage.AddedFailed) : ServiceResponse.CreateSuccess(ResponseMessage.AddedSuccessfully, result);
        }
    }
}
