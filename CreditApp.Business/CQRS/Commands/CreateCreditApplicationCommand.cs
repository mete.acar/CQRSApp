﻿using CreditApp.Business.Response;
using CreditApp.DAL.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.Business.CQRS.Commands
{
    public class CreateCreditApplicationCommand : IRequest<ServiceResponse>
    {
        public int CustomerId { get; set; }

        public double Amount { get; set; }

        public int Maturity { get; set; }

        public decimal InterestRate { get; set; }

        public CreateCreditApplicationCommand(int customerId, double amount, int maturity, decimal interestRate)
        {
            CustomerId = customerId;
            Amount = amount;
            Maturity = maturity;
            InterestRate = interestRate;

        }
    }
}
