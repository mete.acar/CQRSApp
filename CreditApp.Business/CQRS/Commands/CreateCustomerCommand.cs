﻿using CreditApp.Business.Response;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.Business.CQRS.Commands
{
    public class CreateCustomerCommand : IRequest<ServiceResponse>
    {
        public string Name { get; set; }

        public string SurName { get; set; }

        public string IdentityNumber { get; set; }

        public string Email { get; set; }

        public int CreditScore { get; set; }

        public double MonthlyPayment { get; set; }

        public double MonthlyDebt { get; set; }

        public CreateCustomerCommand(string name, string surName, string identityNumber, string email, int creditScore, double monthlyPayment, double monthlyDebt)
        {
            Name = name;
            SurName = surName;
            IdentityNumber = identityNumber;
            Email = email;
            CreditScore = creditScore;
            MonthlyPayment = monthlyPayment;
            MonthlyDebt = monthlyDebt;
        }
    }
}
