﻿using CreditApp.Business.Response;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.Business.CQRS.Commands
{
    public class UpdateResultTypeToReviewCreditApplicationCommand : IRequest<ServiceResponse>
    { 
    }
}
