﻿using System;
using System.Collections.Generic;

namespace CreditApp.DAL.Entities;

public partial class CreditApplication
{
    public int Id { get; set; }

    public int CustomerId { get; set; }

    public double Amount { get; set; }

    public int Maturity { get; set; }

    public decimal InterestRate { get; set; }

    public DateTime ApplicationDate { get; set; }

    public byte Result { get; set; }

    public virtual Customer Customer { get; set; } = null!;
}
