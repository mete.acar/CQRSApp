﻿using System;
using System.Collections.Generic;

namespace CreditApp.DAL.Entities;

public partial class Customer
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string SurName { get; set; } = null!;

    public string IdentityNumber { get; set; } = null!;

    public string Email { get; set; } = null!;

    public int CreditScore { get; set; }

    public double MonthlyPayment { get; set; }

    public double MonthlyDebt { get; set; }

    public virtual ICollection<CreditApplication> CreditApplications { get; set; } = new List<CreditApplication>();
}
