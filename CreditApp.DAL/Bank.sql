USE [Bank]
GO
/****** Object:  Table [dbo].[CreditApplication]    Script Date: 12/7/2023 3:02:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CreditApplication](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[Amount] [float] NOT NULL,
	[Maturity] [int] NOT NULL,
	[InterestRate] [decimal](18, 4) NOT NULL,
	[ApplicationDate] [datetime] NOT NULL,
	[Result] [tinyint] NOT NULL,
 CONSTRAINT [PK_CreditApplication] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 12/7/2023 3:02:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[SurName] [nvarchar](150) NOT NULL,
	[IdentityNumber] [nvarchar](11) NOT NULL,
	[Email] [nvarchar](150) NOT NULL,
	[CreditScore] [int] NOT NULL,
	[MonthlyPayment] [float] NOT NULL,
	[MonthlyDebt] [float] NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CreditApplication] ADD  CONSTRAINT [DF_CreditApplication_Result]  DEFAULT ((0)) FOR [Result]
GO
ALTER TABLE [dbo].[CreditApplication]  WITH CHECK ADD  CONSTRAINT [FK_CreditApplication_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[CreditApplication] CHECK CONSTRAINT [FK_CreditApplication_Customer]
GO
