﻿using CreditApp.DAL.Dtos;
using CreditApp.DAL.Entities; 

namespace CreditApp.DAL.Repositories.Abstract
{
    public interface ICreditApplicationRepository
    {
        public Task<CreditApplication> AddCreditApplicationAsync(CreditApplication creditApplication);
        public Task<List<GetCreditAppDto>> GetCreditApplicationListAsync();
        public Task<GetCreditAppDto> GetCreditApplicationByIdAsync(int id);

        public Task<List<CreditApplication>> GetReviewCreditApplicationList();

        Task UpdateCreditApplication(CreditApplication creditApplication);
    }
}
