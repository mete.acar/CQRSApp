﻿using CreditApp.DAL.DatabaseContext;
using CreditApp.DAL.Entities;
using CreditApp.DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.DAL.Repositories.Concrete
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly BankContext _context;
        public CustomerRepository(BankContext context)
        {
            _context = context;
        } 
        public async Task<Customer> AddCustomerAsync(Customer customer)
        {
             _context.Customers.AddAsync(customer);
            await _context.SaveChangesAsync();
            return customer;
        }

    }
}
