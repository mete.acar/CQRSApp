﻿using CreditApp.DAL.DatabaseContext;
using CreditApp.DAL.Dtos;
using CreditApp.DAL.Entities;
using CreditApp.DAL.Enums;
using CreditApp.DAL.Repositories.Abstract;
using Microsoft.EntityFrameworkCore; 

namespace CreditApp.DAL.Repositories.Concrete
{

    public class CreditApplicationRepository : ICreditApplicationRepository
    {

        private readonly BankContext _context;
        public CreditApplicationRepository(BankContext context)
        {
            _context = context;
        }

        public async Task<CreditApplication> AddCreditApplicationAsync(CreditApplication creditApplication)
        {
            var result = _context.CreditApplications.Add(creditApplication);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<List<GetCreditAppDto>> GetCreditApplicationListAsync()
        { 
            var result = await _context.CreditApplications.Include("Customer").Select(s => new GetCreditAppDto()
            {
                Name = s.Customer.Name,
                SurName = s.Customer.SurName,
                IdentityNumber = s.Customer.IdentityNumber,
                Email = s.Customer.Email,
                Amount = s.Amount,
                Maturity = s.Maturity,
                InterestRate = s.InterestRate,
                ApplicationDate = s.ApplicationDate,
                Result = s.Result,
                ResultDescription = (s.Result == (int)ResultType.Inceleniyor ? "İnceleniyor" : (s.Result == (int)ResultType.KabulEdildi ? "Kabul Edildi" : "Reddedildi"))
            }).ToListAsync(); 
            return result;
        }

        public async Task<GetCreditAppDto> GetCreditApplicationByIdAsync(int id)
        {

            var result = await _context.CreditApplications.Include("Customer").Where(w=> w.Id == id).Select(s => new GetCreditAppDto()
            {
                Name = s.Customer.Name,
                SurName = s.Customer.SurName,
                IdentityNumber = s.Customer.IdentityNumber,
                Email = s.Customer.Email,
                Amount = s.Amount,
                Maturity = s.Maturity,
                InterestRate = s.InterestRate,
                ApplicationDate = s.ApplicationDate,
                Result = s.Result,
                ResultDescription = (s.Result == (int)ResultType.Inceleniyor ? "İnceleniyor" : (s.Result == (int)ResultType.KabulEdildi ? "Kabul Edildi" : "Reddedildi"))
            }).FirstOrDefaultAsync(); 
            return result;
        }

        public async Task<List<CreditApplication>> GetReviewCreditApplicationList()
        {  
            var result = await _context.CreditApplications.Include("Customer").Where(w=> w.Result == (int)ResultType.Inceleniyor).ToListAsync(); 
            return result;

        }
        public async Task UpdateCreditApplication(CreditApplication creditApplication)
        {
            _context.Entry(creditApplication).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
