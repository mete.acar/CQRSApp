﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.DAL.Dtos
{
    public class CreateCreditAppDto
    {

        public int CustomerId { get; set; }

        public double Amount { get; set; }

        public int Maturity { get; set; }

        public decimal InterestRate { get; set; }

    }
}
