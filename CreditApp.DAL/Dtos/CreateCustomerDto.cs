﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.DAL.Dtos
{
    public class CreateCustomerDto
    {
        public string Name { get; set; }

        public string SurName { get; set; }

        public string IdentityNumber { get; set; }

        public string Email { get; set; }

        public int CreditScore { get; set; }

        public double MonthlyPayment { get; set; }

        public double MonthlyDebt { get; set; }
    }
}
