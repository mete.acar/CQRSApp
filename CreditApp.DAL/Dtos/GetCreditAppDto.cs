﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.DAL.Dtos
{
    public class GetCreditAppDto
    {

        public string Name { get; set; }

        public string SurName { get; set; }
        public string IdentityNumber { get; set; }
        public string Email { get; set; }

        public double Amount { get; set; }

        public int Maturity { get; set; }

        public decimal InterestRate { get; set; }

        public DateTime ApplicationDate { get; set; }

        public byte Result { get; set; }
        public string ResultDescription { get; set; }
    }
}
