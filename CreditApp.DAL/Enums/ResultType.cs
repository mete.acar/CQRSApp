﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditApp.DAL.Enums
{
    public enum ResultType
    {
        KabulEdildi = 1,
        Reddedildi = 2,
        Inceleniyor = 0
    }
}
